const articleTemplate = `
<div :class="'article-container'">
    <div v-if="!ready" :class="'loading-bar'">
        <h1>Loading...</h1>
    </div>
    <article v-if="ready" :class="'content-container'">
        <img v-if="data.has_cover === true" :src="getResourceUrl(data.id, data.cover)" :class="'content-cover'"/>
        <div :class="'content-tags'">
            <a v-for="tag in data.tag">{{tag}}</a>
        </div>
        <h1 :class="'content-title'">{{data.title}}</h1>
        <h4 :class="'content-mark'">Author: {{data.author}}</h4>
        <section v-for="section in data.article_content" :class="'section-container'">
            <div v-if="section.type === 'image'" :class="'image-container'">
                <img :src="getResourceUrl(data.id, section.content)" :class="'content-image'"/>
                <p :class="'content-image-comment'">{{section.comment}}</p>
            </div>
            <h3 v-if="section.type === 'subtitle'" :class="'content-subtitle'">{{section.content}}</h3>
            <p v-if="section.type === 'phrase'" v-html="section.content" :class="'content-phrase'"></p>
            <div v-if="section.type === 'reference'" :class="'content-refers'">
                <a v-for="(name, link) in section.content" :href="name" :target="'_blank'">&bull;{{link}}</a>
            </div>
        </section>
        <h4 :class="'post-mark'">Posted on {{data.create_time}}</h4>
    </article>
</div>
`

const article = {
    template: articleTemplate,
    data() {
        return {
            ready: false,
            data: null
        }
    },
    beforeRouteEnter(to, from, next) {
        function getArticleContent() {
            return axios.get(TEMPLUM_API + "article/archive/" + to.params.id);
        }

        axios.all([getArticleContent()])
            .then(axios.spread(function (article) {

                next(vm => {
                    vm.setData(article)
                    vm.setReadyStatus(true)
                })
            }));
    },
    beforeRouteUpdate(to, from, next) {
        this.ready = false;
        this.data = null;
        next()
    },
    methods: {
        setData: function(response) {
            this.data = response.data[0];
        },
        setReadyStatus: function(status) {
            this.ready = status;
        },
        getResourceUrl: function(id, content) {
            return TEMPLUM_CDN + "article/archive/" + id + "/" + content;
        }
    }
}
