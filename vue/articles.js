const articlesTemplate = `
<div :class="'articles-container'">
    <div v-if="!ready" :class="'loading-bar'">
        <h1>Loading...</h1>
    </div>
    <section v-if="ready" v-for="(article) in data" :class="'article-list'">
        <router-link :to="{path: getPath(article.id)}" :class="'article-card'">
            <img :src="getCover(article.id, article.cover)" :class="'article-cover'"/>
            <div :class="'article-label'">
                <h1>{{article.title}}</h1>
                <p>{{article.create_time}}</p>
                <div :class="'tags'">
                    <span v-for="tag in article.tag">{{tag}}</span>
                </div>
            </div>
        </router-link>
    </section>
</div>
`

const articles = {
    template: articlesTemplate,
    data() {
        return {
            ready: false,
            data: null
        }
    },
    beforeRouteEnter(to, from, next) {
        function getArticleList() {
            return axios.get(TEMPLUM_API + "article/archive")
        }

        axios.all([getArticleList()])
            .then(axios.spread(function (data) {

                next(vm => {
                    vm.setData(data)
                    vm.setReadyStatus(true)
                })
            }))
    },
    beforeRouteUpdate(to, from, next) {
        this.ready = false
        this.data = null
        next()
    },
    methods: {
        setData: function(response) {
            this.data = response.data
        },
        setReadyStatus: function(status) {
            this.ready = status
        },
        getCover: function(id, content) {
            return TEMPLUM_CDN + "article/archive/" + id + "/" + content
        },
        getPath: function(id) {
            return "articles/" + id
        }
    }
}
