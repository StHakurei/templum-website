const collectionTemplate = `
<div :class="'present-section'">
    <p :class="'fatego-phrase'">Welcome to my Fate/Grand Order station. I'm a TYPE-MOON fan and amazed by the heroic stories of Fate Universe.
    Here is my current servant collection, usually I will roll for most banner. You could find me via the id, and my friend list is always open.
    Hope all masters have blessed roll in every banner!</p>
    <div :class="'avatar-box'">
        <a v-for="servant in servants" :href="servant.wiki_url" :target="'_blank'">
            <img :src="getServantAvatar(servant.ingame_id)"/>
        </a>
    </div>
</div>
`

const fgoCollection = {
    template: collectionTemplate,
    props: ["servants"],
    methods: {
        getServantAvatar: function(id) {
            return TEMPLUM_CDN + "fatego-usa/avatar/" + id + "/1.png";
        }
    }
}

const galleryTemplate = `
<div :class="'present-section'">
    <p :class="'fatego-phrase'">Heoric Spirit repersents the most exciting story in our history, that is the part I devoted the most in Fate/Grand Order.
    I created this gallery for those who joined my Chaldea in the jorney of FateGO, even after the game, my beloved Heoric Spirits
    could have a place to rest. "In peace I will both lie down and sleep; for you alone, O Lord, make me dwell in safety."</p>
    <div :class="'graph-box'">
        <a v-for="servant in servants" :href="servant.wiki_url" :target="'_blank'" :class="'graph-card'">
            <img :src="getGraph4(servant.ingame_id)" :class="'saint-graph'"/>
            <img :src="getClassIcon(servant.class, servant.rarity)" :class="'class-icon'"/>
            <span>{{servant.name}}</span>
        </a>
    </div>
</div>
`

const fgoGallery = {
    template: galleryTemplate,
    props: ["servants"],
    methods: {
        getGraph4: function(id) {
            return TEMPLUM_CDN + "fatego-usa/graph/" + id + "/4.png";
        },
        getClassIcon: function(className, rarity) {
            let name = className.toLowerCase();
            let color = null;
            switch (rarity) {
                case 1:
                case 2:
                    color = "bronze";
                    break;
                case 3:
                    color = "silver";
                    break;
                case 4:
                case 5:
                    color = "gold";
                    break;
            }
            return TEMPLUM_CDN + "fatego-usa/icon/" + name + "_" + color + ".png";
        }
    }
}

const bannerTemplate = `
<div :class="'banner-section'">
    <div :class="'record-title'">
        <img :src="getEventIcon(record.iconId)"/>
        <div :class="'record-summary'">
            <div>
                <span>Pack:</span>
                <a>{{record.pack}}</a>
            </div>
            <div>
                <span>Saint Quartz:</span>
                <a>{{record.sq}}</a>
            </div>
            <div>
                <span>Ticket:</span>
                <a>{{record.ticket}}</a>
            </div>
            <div>
                <span>Rolling:</span>
                <a>{{record.summoning}}</a>
            </div>
            <div>
                <button v-on:click="showServant">Servants{{servantExpand}}</button>
                <button v-on:click="showEssence">Essences{{essenceExpand}}</button>
            </div>
        </div>
    </div>
    <div :class="'record-detail'">
        <ul v-if="servants">
            <h4>Servants</h4>
            <li v-for="(quatity, name) in record.servant" :class="'record-container'">
                <span :class="'result-name'">{{name}}</span>
                <span :class="'result-quatity'">{{quatity}}</span>
                <span :class="'result-rate'">{{getRate(quatity, record.summoning)}}%</span>
            </li>
        </ul>
        <ul v-if="essences">
            <h4>Craft Essences</h4>
            <li v-for="(quatity, name) in record.craftEssences" :class="'record-container'">
                <span :class="'result-name'">{{name}}</span>
                <span :class="'result-quatity'">{{quatity}}</span>
                <span :class="'result-rate'">{{getRate(quatity, record.summoning)}}%</span>
            </li>
        </ul>
    </div>
</div>
`

const bannerComponent = {
    template: bannerTemplate,
    props: ["record"],
    data() {
        return {
            servants: false,
            essences: false,
            servantExpand: "+",
            essenceExpand: "+"
        }
    },
    methods: {
        getEventIcon: function(id) {
            return TEMPLUM_CDN + "fatego-usa/event/" + id + ".png";
        },
        showServant: function() {
            if(this.servants) {
                this.servants = false;
                this.servantExpand = " +"
            }
            else {
                this.servants = true;
                this.servantExpand = " -"
            }
        },
        showEssence: function() {
            if(this.essences) {
                this.essences = false;
                this.essenceExpand = " +";
            }
            else {
                this.essences = true;
                this.essenceExpand = " -";
            }
        },
        getRate: function(quatity, total) {
            return (quatity / total * 100).toFixed(2);
        }
    }
}

const summontemplate = `
<div :class="'present-section'">
    <img :src="rateup" :class="'summon-cover'"/>
    <section v-for="banner in summons" :class="'banner-box'">
        <h3>&#x2605{{banner.event}}&#x2605</h3>
        <banner-component v-bind:record="banner"></banner-component>
    </section>
</div>
`

const fgoSummon = {
    template: summontemplate,
    components: {
        "banner-component": bannerComponent
    },
    props: ["summons"],
    data() {
        return {
            rateup: "./image/rateuplie.webp"
        }
    }
}

const fategoTemplate = `
<div :class="'fatego-container'">
    <img :src="pageCover" :class="'fatego-cover'"/>
    <nav :class="'fatego-navbar'">
        <button :class="'left-button'" v-on:click="showCollection">Colllection</button>
        <button v-on:click="showGallery">Gallery</button>
        <button :class="'right-button'" v-on:click="showSummon">Summoning</button>
    </nav>
    <h1 :class="'fatego-id'">FateGO USA - 461,757,874</h1>
    <section v-if="ready" :class="'fatego-content-container'">
        <collection-component v-if="collection" v-bind:servants="servantData">
        </collection-component>
        <gallery-component v-if="gallery"  v-bind:servants="servantData">
        </gallery-component>
        <summon-component v-if="banner"  v-bind:summons="summonData">
        </summon-component>
    </section>
</div>
`

const fatego = {
    template: fategoTemplate,
    components: {
        "collection-component": fgoCollection,
        "gallery-component": fgoGallery,
        "summon-component": fgoSummon
    },
    data() {
        return {
            servantData: null,
            summonData: null,
            ready: false,
            pageCover: "./image/fategoCover.webp",
            collection: true,
            gallery: false,
            banner: false
        }
    },
    beforeRouteEnter(to, from, next) {
        function getServantData() {
            return axios.get(TEMPLUM_API + "fatego/servants")
        }

        function getSummonData() {
            return axios.get(TEMPLUM_API + "fatego/summons")
        }

        axios.all([getServantData(), getSummonData()])
            .then(axios.spread(function (servants, summons) {
                next(vm => {
                    vm.setServantData(servants)
                    vm.setSummonData(summons)
                    vm.setReadyStatus(true)
                })
            }));
    },
    beforeRouteUpdate(to, from, next) {
        this.servantData = null
        this.summonData = null
        this.ready = true
        next()
    },
    methods: {
        setServantData: function(response) {
            this.servantData = response.data;
        },
        setSummonData: function(response) {
            this.summonData = response.data;
        },
        setReadyStatus: function(status) {
            this.ready = status;
        },
        showCollection: function() {
            this.collection = true
            this.gallery = false
            this.banner = false
        },
        showGallery: function() {
            this.collection = false
            this.gallery = true
            this.banner = false
        },
        showSummon: function() {
            this.collection = false
            this.gallery = false
            this.banner = true
        }
    }
}
