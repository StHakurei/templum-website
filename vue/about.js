const aboutArticle = {
    template: articleTemplate,
    data() {
        return {
            ready: false,
            data: null
        }
    },
    beforeRouteEnter(to, from, next) {
        function getArticleContent() {
            return axios.get(TEMPLUM_API + "article/archive/5edf7da23c18542684397827");
        }

        axios.all([getArticleContent()])
            .then(axios.spread(function (article) {

                next(vm => {
                    vm.setData(article)
                    vm.setReadyStatus(true)
                })
            }));
    },
    beforeRouteUpdate(to, from, next) {
        this.ready = false;
        this.data = null;
        next()
    },
    methods: {
        setData: function(response) {
            this.data = response.data[0];
        },
        setReadyStatus: function(status) {
            this.ready = status;
        },
        getResourceUrl: function(id, content) {
            return TEMPLUM_CDN + "article/archive/" + id + "/" + content;
        }
    }
}
